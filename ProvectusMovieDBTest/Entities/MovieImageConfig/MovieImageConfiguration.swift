//
//  MovieImageConfiguration.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

struct MovieImageConfiguration: Codable {
    var baseUrl: String! = ""
    var secureBaseUrl: String! = ""
    var backdropSizes: [String]? = nil
    var logoSizes: [String]? = nil
    var posterSizes: [String]? = nil
    var profileSizes: [String]? = nil
    var stillSizes: [String]? = nil
}

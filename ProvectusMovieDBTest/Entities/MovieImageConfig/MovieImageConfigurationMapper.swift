//
//  MovieModelMapper.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 01.09.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class MovieImageConfigurationMapper {
    func map(dictionary: Dictionary<String, Any>) -> MovieImageConfiguration? {
        
        guard let  result = dictionary["images"] as? Dictionary<String, Any> else{
            return nil
        }
        
        var movieImageConfig = MovieImageConfiguration()
        
        movieImageConfig.baseUrl        = result["base_url"] as! String
        movieImageConfig.secureBaseUrl  = result["secure_base_url"] as! String
        movieImageConfig.backdropSizes  = result["backdrop_sizes"] as? [String]
        movieImageConfig.logoSizes      = result["logo_sizes"] as? [String]
        movieImageConfig.posterSizes    = result["poster_sizes"] as? [String]
        movieImageConfig.profileSizes   = result["profile_sizes"] as? [String]
        movieImageConfig.stillSizes     = result["still_sizes"] as? [String]
        
        return movieImageConfig
    }
}

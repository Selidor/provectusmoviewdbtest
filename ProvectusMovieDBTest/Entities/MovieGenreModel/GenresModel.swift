//
//  GenresModel.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 01.09.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

struct GenresModel: Codable {
    var genreID: Int! = 0
    var name : String! = ""
}

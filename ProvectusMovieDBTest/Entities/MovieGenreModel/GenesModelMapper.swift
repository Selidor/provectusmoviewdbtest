//
//  GenesModelMapper.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 01.09.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class GenresModelMapper {
    
    func map(dictionary: Dictionary<String, Any>) -> GenresModel? {
        
        let genresModel = GenresModel(genreID:  dictionary["id"] as! Int,
                                      name:     dictionary["name"] as! String)
        return genresModel;
    }
    
    func map(genres: Dictionary<String, Any>) -> [GenresModel] {
        
        guard let results = genres["genres"] as? Array<Dictionary<String, Any>> else{
            return []
        }
        
        var genresModels = Array<GenresModel>()
        
        for (_, element) in results.enumerated() {
            let model = map(dictionary: element);
            if (model != nil) {
                genresModels.append(model!)
            }
        }
        return genresModels;
    }
}

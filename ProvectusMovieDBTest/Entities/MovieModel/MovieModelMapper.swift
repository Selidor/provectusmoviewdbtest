//
//  MovieModelMapper.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 01.09.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class MovieModelMapper {
    
    func map(dictionary: Dictionary<String, Any>,
             imageConfigurator: MovieImageConfigurator,
             genresManager: GenresManager) -> MovieModel? {
        
        let originalTitle = dictionary["original_title"] as? String
        let title = dictionary["title"] as? String
        let release_date = dictionary["release_date"] as? String
        
        var movieModel = MovieModel()
        
        //Required fields
        movieModel.movieID =        (dictionary["id"] as? NSNumber)!
        movieModel.originalTitle =  ((originalTitle != nil) ? originalTitle : dictionary["original_name"] as? String)!
        
        //Optional fields
        movieModel.title =          (title != nil) ? title : dictionary["name"] as? String
        movieModel.releaseDate =    (release_date != nil) ? release_date : dictionary["first_air_date"] as? String
        movieModel.backDropPath =   imageConfigurator.backDropImageFullPath(imagePath: dictionary["backdrop_path"] as? String)
        
        let posterPath = dictionary["poster_path"] as? String
        
        if(posterPath != nil){
            movieModel.posterPathBig = imageConfigurator.posterImageFullPath(size: .big, imagePath: posterPath)
            movieModel.posterPathLittle = imageConfigurator.posterImageFullPath(size: .little, imagePath: posterPath)
        }
        
        movieModel.genres =         genresManager.genresNameAtIDs(genreIDs: dictionary["genre_ids"] as? [NSNumber])
        movieModel.overview =       dictionary["overview"] as? String
        movieModel.rating =         dictionary["vote_average"] as? NSNumber
        
        return movieModel
    }
    
    func map(movies: Dictionary<String, Any>,
             imageConfigurator: MovieImageConfigurator,
             genresManager: GenresManager) -> [MovieModel] {
        guard let results = movies["results"] as? Array<Dictionary<String, Any>> else{
            return []
        }
        
        var moviesModels = Array<MovieModel>()
        
        for (_, element) in results.enumerated() {
            let model = map(dictionary: element,
                            imageConfigurator: imageConfigurator,
                            genresManager: genresManager);
            if(model != nil){
                moviesModels.append(model!)
            }
        }
        
        return moviesModels
    }
    
    func convert(fromManagedObject managedObject: Movie) -> MovieModel {
        
        var movieModel = MovieModel()
        
        //Required fields
        movieModel.movieID =            NSNumber(integerLiteral: Int(managedObject.movieID))
        movieModel.originalTitle =      managedObject.originalTitle!
        
        //Optional fields
        movieModel.title =              managedObject.title
        movieModel.releaseDate =        managedObject.releaseDate
        movieModel.backDropPath =       managedObject.backDropPath
        movieModel.posterPathBig =      managedObject.posterPathBig
        movieModel.posterPathLittle =   managedObject.posterPathLittle
        movieModel.genres =             managedObject.genres as? [String]
        movieModel.overview =           managedObject.overview
        movieModel.rating =             NSNumber(floatLiteral: Double(managedObject.rating))
        
        return movieModel
    }
    
    func convert(fromManagedObjects managedObjects: [Movie]) -> [MovieModel] {
        
        var movieModels = Array<MovieModel>()
        
        for Movie in managedObjects {
            movieModels.append(convert(fromManagedObject: Movie))
        }
        
        return movieModels
    }
    
}

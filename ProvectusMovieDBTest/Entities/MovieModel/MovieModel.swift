//
//  MovieModel.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit
import Foundation

struct MovieModel {
    
    var movieID: NSNumber! = 0
    var originalTitle: String! = ""
    var title: String? = nil
    var backDropPath: String? = nil
    var genres: [String]? = nil
    var overview: String? = nil
    var posterPathLittle: String? = nil
    var posterPathBig: String? = nil
    var releaseDate: String? = nil
    var rating: NSNumber! = 0
}


//
//  MovieDetailWireFrame.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class MovieDetailWireFrame: MovieDetailWireframeProtocol {
    func configureModule(viewController: MovieDetailViewController) {
        
        let presenter = MovieDetailPresenter(view: viewController)
        let interactor = MovieDetailInteractor(presenter: presenter)
        let router = MovieDetailRouter(viewController: viewController)
        
        viewController.presenter = presenter;
        presenter.interactor = interactor;
        presenter.router = router;
    }
}

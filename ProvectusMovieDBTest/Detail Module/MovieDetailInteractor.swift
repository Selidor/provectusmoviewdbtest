//
//  MovieDetailInteractor.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class MovieDetailInteractor: MovieDetailInteractorProtocol {

    weak var presenter: MovieDetailPresenterProtocol!
    
    required init(presenter: MovieDetailPresenterProtocol) {
        self.presenter = presenter
    }
}

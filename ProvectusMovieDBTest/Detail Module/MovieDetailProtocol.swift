//
//  MovieDetailProtocol.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import Foundation
import UIKit

protocol MovieDetailViewControllerProtocol: class {
    
    var selectedMovie: MovieModel! { set get }
}

protocol MovieDetailPresenterProtocol: class {
    
    func configureView(with movie: MovieModel)
}

protocol MovieDetailInteractorProtocol: class {
    
}

protocol MovieDetailRouterProtocol: class {
    
    func dismiss()
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}

protocol MovieDetailWireframeProtocol: class {
    func configureModule(viewController: MovieDetailViewController)
}

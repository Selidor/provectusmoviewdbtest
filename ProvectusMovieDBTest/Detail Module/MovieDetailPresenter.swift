//
//  MovieDetailPresenter.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class MovieDetailPresenter: MovieDetailPresenterProtocol {
    
    weak var view: MovieDetailViewController!
    var interactor: MovieDetailInteractorProtocol!
    var router: MovieDetailRouterProtocol!
    
    required init(view: MovieDetailViewController) {
        self.view = view
    }
    
    func configureView(with movie: MovieModel) {
        
    }
    
}

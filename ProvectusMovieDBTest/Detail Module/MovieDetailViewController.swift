//
//  MovieDetailViewController.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieDetailViewController: UIViewController, MovieDetailViewControllerProtocol {
    
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var overviewLabel: UILabel!
    
    var selectedMovie: MovieModel!
    var presenter: MovieDetailPresenter!
    let wireframe: MovieDetailWireframeProtocol = MovieDetailWireFrame()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wireframe.configureModule(viewController: self)
        
        movieTitleLabel.text = selectedMovie.title
        ratingLabel.text = String(format: "%.1f", selectedMovie.rating.doubleValue)
        let genresStringRepresent = selectedMovie.genres?.joined(separator: ", ")
        genresLabel.text = genresStringRepresent
        overviewLabel.text = selectedMovie.overview
        
        if(selectedMovie.posterPathBig != nil){
            let url = URL(string: selectedMovie.posterPathBig!)!
            self.posterImageView.af_setImage(withURL: url)
        }
        
        presenter.configureView(with: selectedMovie)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        presenter.router.prepare(for: segue, sender: sender)
    }
}

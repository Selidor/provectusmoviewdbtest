//
//  MovieDetailRouter.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class MovieDetailRouter: MovieDetailRouterProtocol {
    
    weak var viewController: MovieDetailViewController!
    
    init(viewController: MovieDetailViewController) {
        self.viewController = viewController
    }
    
    func dismiss() {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    

}

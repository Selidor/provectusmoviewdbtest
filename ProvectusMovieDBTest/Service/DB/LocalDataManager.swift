//
//  LocalDataManager.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 01.09.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit
import CoreData

enum PersistenceError: Error {
    case managedObjectContextNotFound
    case couldNotSaveObject
    case objectNotFound
}

class LocalDataManager {
    
    let imageConfigKey = "imageConfig"
    let genresKey = "genresKey"
    
    func saveImageConfiguration(imageConfiguration: MovieImageConfiguration) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(imageConfiguration), forKey:self.imageConfigKey)
    }
    
    func getImageConfiguration(onSuccess success: @escaping Constants.imageConfigSuccessBlock,
                               onFailure fail: @escaping Constants.failureBlock) {
        let queue = DispatchQueue.global(qos: .utility)
        queue.async {
            if let data = UserDefaults.standard.value(forKey:self.imageConfigKey) as? Data {
                
                let imageConfig = try? PropertyListDecoder().decode(MovieImageConfiguration.self, from: data)
                
                DispatchQueue.main.async {
                    if(imageConfig != nil){
                        success(imageConfig)
                    }else{
                        fail(nil)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    fail(nil)
                }
            }
        }
    }
    
    func saveGenres(genres: [GenresModel]) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(genres), forKey:self.genresKey)
    }
    
    func getMovieGenres(onSuccess success: @escaping Constants.movieGenresSuccessBlock,
                        onFailure fail: @escaping Constants.failureBlock){
        
        let queue = DispatchQueue.global(qos: .utility)
        queue.async {
            if let data = UserDefaults.standard.value(forKey:self.genresKey) as? Data {
                
                let genres = try? PropertyListDecoder().decode(Array<GenresModel>.self, from: data)
                DispatchQueue.main.async {
                    if(genres != nil){
                        success(genres!)
                    }else{
                        fail(nil)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    fail(nil)
                }
            }
        }
    }

    func saveTrendingMovies(movies: [MovieModel]) throws {
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistenceError.managedObjectContextNotFound
        }
        
        for movieModel in movies {
            if(!movieExists(id: movieModel.movieID.intValue, moc: managedOC)){
                if let newMovie = NSEntityDescription.entity(forEntityName: String(describing: Movie.self), in: managedOC){
                    let movie = Movie(entity: newMovie, insertInto: managedOC)
                    movie.genres = movieModel.genres as NSObject?
                    movie.movieID = movieModel.movieID.int32Value
                    movie.backDropPath = movieModel.backDropPath
                    movie.posterPathBig = movieModel.posterPathBig
                    movie.posterPathLittle = movieModel.posterPathLittle
                    movie.title = movieModel.title
                    movie.originalTitle = movieModel.originalTitle
                    movie.overview = movieModel.overview
                    movie.releaseDate = movieModel.releaseDate
                    movie.rating = movieModel.rating.doubleValue
                }
            }
        }
        do {
            try managedOC.save()
        } catch {
            throw PersistenceError.couldNotSaveObject
        }
    }
    
    func getTrendingMovieList(search query: String) throws -> [Movie] {
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistenceError.managedObjectContextNotFound
        }
        
        let request: NSFetchRequest<Movie> = NSFetchRequest(entityName: String(describing: Movie.self))
        
        if(query.count > 0){
            var predicate = NSPredicate()
            predicate = NSPredicate(format: "title contains[c] %@", query)
            request.predicate = predicate
        }
        
        return try managedOC.fetch(request)
    }
    
    func movieExists(id: Int, moc: NSManagedObjectContext) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Movie")
        fetchRequest.includesSubentities = false
        
        var predicate = NSPredicate()
        predicate = NSPredicate(format: "movieID == %i", id)
        fetchRequest.predicate = predicate
        var entitiesCount = 0
        
        do {
            entitiesCount = try moc.count(for: fetchRequest)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        return entitiesCount > 0
    }
}

//
//  CoreDataStore.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit
import CoreData

class CoreDataStore {
    static var persistentStoreCoordinator: NSPersistentStoreCoordinator? {
        return AppDelegate.sharedInstance().persistentContainer.persistentStoreCoordinator
    }
    
    static var managedObjectModel: NSManagedObjectModel? {
        return AppDelegate.sharedInstance().persistentContainer.managedObjectModel
    }
    
    static var managedObjectContext: NSManagedObjectContext? {
        return AppDelegate.sharedInstance().persistentContainer.viewContext
    }
}

//
//  MovieDBAPIManager.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//
import UIKit
import Alamofire

class MovieDBAPIManager {
    
    let baseURLstring = "https://api.themoviedb.org/3/"
    let apiKey = "902c41bf11919eb861e8b632a1105884"
    let configurationEndPoint = "configuration"
    let trendingWeekEndPoint = "trending/all/week"
    let searchEndPoint = "search/movie"
    let genresEndPoint = "genre/movie/list"
    
    func getImageConfiguration(onSuccess success: @escaping Constants.imageConfigSuccessBlock,
                               onFailure fail: @escaping Constants.failureBlock) {
        
        let parameters = ["api_key": apiKey]
        
        Alamofire.request(baseURLstring + configurationEndPoint,
                          method: .get,
                          parameters: parameters,
                          encoding: URLEncoding.default,
                          headers: nil).validate().responseJSON { response in
                            switch response.result{
                            case .success(let value):
                                
                                guard let jsonDictionary = value as? Dictionary<String, Any> else {
                                    fail(nil)
                                    return
                                }
                                
                                let imageConfig = MovieImageConfigurationMapper().map(dictionary:jsonDictionary)
                                success (imageConfig)
                                
                            case .failure(let error):
                                fail(error)
                            }
        }
    }
    
    func getMovieGenres(onSuccess success: @escaping Constants.movieGenresSuccessBlock,
                        onFailure fail: @escaping Constants.failureBlock) {
        let parameters = ["api_key": apiKey]
        
        Alamofire.request(baseURLstring + genresEndPoint,
                          method: .get,
                          parameters: parameters,
                          encoding: URLEncoding.default,
                          headers: nil).validate().responseJSON { response in
                            switch response.result{
                            case .success(let value):
                                
                                guard let jsonDictionary = value as? Dictionary<String, Any> else {
                                    fail(nil)
                                    return
                                }
                                let genresModels = GenresModelMapper().map(genres: jsonDictionary)
                                success(genresModels)
                                
                            case .failure(let error):
                                fail(error)
                            }
        }
    }
    
    func getTrendingMovieList(imageConfigurator: MovieImageConfigurator,
                              genresManager: GenresManager,
                              onSuccess success: @escaping Constants.movieListSuccessBlock,
                              onFailure fail: @escaping Constants.failureBlock){
        
        let parameters = ["api_key": apiKey]
        getMovieList(imageConfigurator: imageConfigurator,
                     genresManager: genresManager,
                     params: parameters,
                     urlString: baseURLstring + trendingWeekEndPoint, onSuccess: success, onFailure: fail)
    }
    
    private func getMovieList(imageConfigurator: MovieImageConfigurator,
                              genresManager: GenresManager,
                              params: Dictionary<String, Any>, urlString: String, onSuccess success: @escaping Constants.movieListSuccessBlock, onFailure fail: @escaping Constants.failureBlock){
        Alamofire.request(urlString,
                          method: .get,
                          parameters: params,
                          encoding: URLEncoding.default,
                          headers: nil).validate().responseJSON { (response) in
                            switch response.result{
                            case .success(let value):
                                
                                guard let jsonDictionary = value as? Dictionary<String, Any> else {
                                    fail(nil)
                                    return
                                }
                                
                                let movies  = MovieModelMapper().map(movies: jsonDictionary, imageConfigurator: imageConfigurator, genresManager: genresManager)
                                success(movies)
                                
                            case .failure(let error):
                                fail(error)
                            }
        }
    }
    
    func searchMovie(imageConfigurator: MovieImageConfigurator,
                     genresManager: GenresManager,
                     searchText query: String,
                     onSuccess success: @escaping Constants.movieListSuccessBlock,
                     onFailure fail: @escaping Constants.failureBlock){
        let parameters = ["api_key": apiKey, "query" : query]
        getMovieList(imageConfigurator: imageConfigurator,
                     genresManager: genresManager,
                     params: parameters,
                     urlString: baseURLstring + searchEndPoint,
                     onSuccess: success,
                     onFailure: fail)
    }
}

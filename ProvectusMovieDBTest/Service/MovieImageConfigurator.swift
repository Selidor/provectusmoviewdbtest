//
//  MovieImageConfigurator.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 03.09.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

enum ImageSize: Int {
    case little
    case big
}

class MovieImageConfigurator{
    
    var imageConfiguration: MovieImageConfiguration!
    
    init(with imageConfigurations: MovieImageConfiguration) {
        self.imageConfiguration = imageConfigurations
    }
    
    func posterImageFullPath(size: ImageSize, imagePath: String?) -> String? {
        guard (imagePath != nil) else {
            return imagePath
        }
        
        var fullPath = ""
        
        let sizes = imageConfiguration.posterSizes
        
        switch size {
        case .little:
            fullPath = imageConfiguration.secureBaseUrl + (sizes?.first)! + imagePath!
        case .big:
            let bigSize = sizes?[(sizes?.count)! - 2]
            fullPath = imageConfiguration.secureBaseUrl + bigSize! + imagePath!
        }
        return fullPath
    }
    
    func backDropImageFullPath(imagePath: String?) -> String? {
        guard (imagePath != nil) else {
            return imagePath
        }
        let fullPath = imageConfiguration.secureBaseUrl + (imageConfiguration.backdropSizes?.last)! + imagePath!
        return fullPath
    }
    
    func logoImageFullPath(imagePath: String?) -> String? {
        guard (imagePath != nil) else {
            return imagePath
        }
        let fullPath = imageConfiguration.secureBaseUrl + (imageConfiguration.logoSizes?.last)! + imagePath!
        return fullPath
    }
}

//
//  GenresManager.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 03.09.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

enum GenreError: Error {
    case GenreNotFound
}

class GenresManager: NSObject {
    var genres: [GenresModel]!
    
    init(with genres: [GenresModel]) {
        self.genres = genres
    }
    
    func genreNameAtID(genreID: NSNumber) throws -> String {
        for genre in genres {
            if (genre.genreID == genreID.intValue){
                return genre.name!
            }
        }
        
        throw GenreError.GenreNotFound
    }
    
    func genresNameAtIDs(genreIDs: [NSNumber]?) -> [String]? {
        
        guard (genreIDs != nil) else {
            return nil
        }
        
        var names = [String]()
        for genreID in genreIDs! {
            let genreName = try? genreNameAtID(genreID: genreID)
            if(genreName != nil){
                names.append(genreName!)
            }
        }
        
        return names
    }
}

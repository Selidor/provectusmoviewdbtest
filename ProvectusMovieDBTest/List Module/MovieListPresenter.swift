//
//  MovieListPresenter.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class MovieListPresenter: MovieListPresenterProtocol {

    weak var view: MovieListViewController!
    var interactor: MovieListInteractorProtocol!
    var router: MovieListRouterProtocol!
    
    required init(view: MovieListViewController) {
        self.view = view
    }
    
    func configureView() {
        view.showLoading()
        interactor.loadData()
    }
    
    func didLoadData(data: [MovieModel]) {
        view.hideLoading()
        view.showData(with: data)
    }
    
    func searchTextDidChange(searchText: String) {
        view.showLoading()
        interactor.search(text: searchText)
    }
    
    func onError(error: NSError) {
        view.showError(error: error)
    }
}

//
//  MovieListRouter.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class MovieListRouter: MovieListRouterProtocol {
    
    weak var viewController: MovieListViewController!
    
    init(viewController: MovieListViewController) {
        self.viewController = viewController
    }
    
    func showDetailScene() {
        
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! MovieDetailViewControllerProtocol
        let sourceVC = segue.source as! MovieListViewControllerProtocol
        destinationVC.selectedMovie = sourceVC.selectedMovie
    }
}

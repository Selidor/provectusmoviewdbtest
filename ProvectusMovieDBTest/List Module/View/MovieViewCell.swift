//
//  MovieViewCell.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieViewCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView!.image = nil
    }
    
    func configure(with movie: MovieModel) {
        titleLabel.text = movie.title
        ratingLabel.text = String(format: "%.1f", movie.rating.doubleValue)
        let background = DispatchQueue.global()
        
        guard movie.posterPathLittle != nil else{
            return
        }
        
        let url = URL(string: movie.posterPathLittle!)!
        self.iconImageView!.af_setImage(withURL: url,
                                        placeholderImage: nil,
                                        filter: nil,
                                        progress: nil,
                                        progressQueue: background,
                                        imageTransition: .crossDissolve(0.15),
                                        runImageTransitionIfCached: false) { (image) in
                                            
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

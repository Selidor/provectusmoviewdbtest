//
//  MovieListViewController.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit
import PKHUD

@objcMembers class MovieListViewController: UIViewController, MovieListViewControllerProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    
    var refreshControl: UIRefreshControl!
    var presenter: MovieListPresenter!
    let wireframe: MovieListWireframeProtocol = MovieListWireframe()
    var movies: [MovieModel] = []
    var selectedMovie: MovieModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        wireframe.configureModule(viewController: self)
        
        refreshControl = UIRefreshControl()
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        tableView.tableFooterView = UIView()
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Movies"
        navigationItem.searchController = self.searchController
        definesPresentationContext = true
        
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        presenter.configureView()
    }
    
    func showData(with posts: [MovieModel]) {
        movies = posts
        tableView.reloadData()
    }
    
    @objc func refresh(){
        presenter.configureView()
    }
    
    func showLoading() {
        refreshControl.beginRefreshing()
        //HUD.show(.progress)
    }
    
    func hideLoading() {
        //HUD.hide()
        refreshControl.endRefreshing()
    }
    
    func showError(error: NSError) {
        HUD.flash(.label("Error"), delay: 2.0)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        presenter.router .prepare(for: segue, sender: sender)
    }
    
    func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0)
        }
    }
    
    func keyboardWillHide(notification: Notification) {
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
    }
}

extension MovieListViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
    }
}

extension MovieListViewController: UISearchBarDelegate{
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        presenter.configureView()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBar.showsCancelButton = true
        presenter.searchTextDidChange(searchText: searchText)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
}
extension MovieListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieViewCell", for: indexPath) as! MovieViewCell
        let movie = movies[indexPath.row]
        cell.configure(with: movie)
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMovie = movies[indexPath.row]
        performSegue(withIdentifier: "showDetail", sender: self)
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

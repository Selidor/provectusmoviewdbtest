//
//  MovieListInteractor.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class MovieListInteractor: MovieListInteractorProtocol {
    
    weak var presenter: MovieListPresenterProtocol!
    var apiManager: MovieDBAPIManager!
    var localDataManager: LocalDataManager!
    var imageConfigurator: MovieImageConfigurator!
    var genresManager: GenresManager!
    
    required init(presenter: MovieListPresenterProtocol) {
        self.presenter = presenter
    }
    
    func loadData() {
        weak var weakSelf = self
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let group = DispatchGroup()
            var groupError: NSError?
            
            group.enter()
            weakSelf!.loadImageConfiguration(onSuccess: { (imageConfiguration) in
                weakSelf!.imageConfigurator = MovieImageConfigurator(with: imageConfiguration!)
                group.leave()
            }) { (error) in
                groupError = NSError()
                group.leave()
            }
            
            group.enter()
            weakSelf!.loadGenres(onSuccess: { (genres) in
                weakSelf!.genresManager = GenresManager(with: genres)
                group.leave()
            }) { (error) in
                groupError = NSError()
                group.leave()
            }
            
            group.wait()
            
            if(groupError != nil){
                DispatchQueue.main.async {
                    weakSelf!.presenter.onError(error: groupError!)
                }
            }
            
            weakSelf!.loadMovieList(search: "", onSuccess: { (movies) in
                let srtMovies = movies.sorted(by: { $0.rating.intValue > $1.rating.intValue })
                DispatchQueue.main.async {
                    weakSelf!.presenter.didLoadData(data: srtMovies)
                }
            }) { (error) in
                groupError = NSError()
                weakSelf!.presenter.onError(error: groupError!)
            }
        }
    }
    
    func loadImageConfiguration(onSuccess success: @escaping Constants.imageConfigSuccessBlock,
                                onFailure fail: @escaping Constants.failureBlock){
        weak var weakSelf = self
        
        localDataManager.getImageConfiguration(onSuccess: success) { (error) in
            if(NetworkManager.shared.reachabilityManager?.isReachable == true){
                weakSelf!.apiManager.getImageConfiguration(onSuccess: { MovieImageConfig in
                    weakSelf!.localDataManager.saveImageConfiguration(imageConfiguration: MovieImageConfig!)
                    success(MovieImageConfig)
                }, onFailure: fail)
            }else{
                fail(nil)
            }
        }
    }
    
    func loadGenres(onSuccess success: @escaping Constants.movieGenresSuccessBlock,
                    onFailure fail: @escaping Constants.failureBlock){
        
        weak var weakSelf = self
        
        localDataManager.getMovieGenres(onSuccess: success, onFailure: { (error) in
            if(NetworkManager.shared.reachabilityManager?.isReachable == true){
                weakSelf!.apiManager.getMovieGenres(onSuccess: { (genres) in
                    weakSelf!.localDataManager.saveGenres(genres: genres)
                    success(genres)
                }, onFailure: fail)
            }else{
                fail(nil)
            }
        })
    }
    
    func loadMovieList(search query: String ,
                       onSuccess success: @escaping Constants.movieListSuccessBlock,
                       onFailure fail: @escaping Constants.failureBlock){
        weak var weakSelf = self
        
        if(NetworkManager.shared.reachabilityManager!.isReachable == true){
            if(query.count > 0){
                weakSelf!.apiManager.searchMovie(imageConfigurator: imageConfigurator,
                                                 genresManager: genresManager,
                                                 searchText: query,
                                                 onSuccess: success,
                                                 onFailure: fail)
            }else{
                weakSelf!.apiManager.getTrendingMovieList(imageConfigurator: imageConfigurator,
                                                          genresManager: genresManager,
                                                          onSuccess: { (movies) in
                                                            do{
                                                                try weakSelf!.localDataManager.saveTrendingMovies(movies: movies)
                                                                success(movies)
                                                            }catch{
                                                                print("Error")
                                                                fail(nil)
                                                            }
                }, onFailure: fail)
            }
        }else{
            DispatchQueue.main.async {
                do{
                    let movies = try weakSelf!.localDataManager.getTrendingMovieList(search: query)
                    success(MovieModelMapper().convert(fromManagedObjects: movies))
                }catch{
                    print("Error")
                    fail(nil)
                }
            }
        }
    }
    
    func search(text: String) {
        
        weak var weakSelf = self
        
        loadMovieList(search: text, onSuccess: { (movies) in
            let srtMovies = movies.sorted(by: { $0.rating.intValue > $1.rating.intValue })
            DispatchQueue.main.async {
                weakSelf!.presenter.didLoadData(data: srtMovies)
            }
        }) { (error) in
            weakSelf!.presenter.onError(error:
                NSError())
        }
    }
}

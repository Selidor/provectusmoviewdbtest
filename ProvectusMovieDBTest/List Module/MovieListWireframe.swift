//
//  MovieListWireframe.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class MovieListWireframe: MovieListWireframeProtocol {
    func configureModule(viewController: MovieListViewController) {
        
        let presenter = MovieListPresenter(view: viewController)
        let interactor = MovieListInteractor(presenter: presenter)
        let router = MovieListRouter(viewController: viewController)
        let localDataManager = LocalDataManager()
        let apiManager = MovieDBAPIManager()
        
        interactor.apiManager = apiManager
        interactor.localDataManager = localDataManager
        viewController.presenter = presenter;
        presenter.interactor = interactor;
        presenter.router = router;
    }
}

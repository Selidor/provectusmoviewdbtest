//
//  MovieListProtocol.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 31.08.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import Foundation
import UIKit

protocol MovieListViewControllerProtocol: class {
    
    var selectedMovie: MovieModel? { set get }
    
    func showData(with posts: [MovieModel])
    
    func showLoading()
    
    func hideLoading()
    
    func showError(error: NSError)
}

protocol MovieListPresenterProtocol: class {
    
    var router: MovieListRouterProtocol! { set get }
    
    func configureView()
    
    func didLoadData(data: [MovieModel])
    
    func searchTextDidChange(searchText: String)
    
    func onError(error: NSError)
}

protocol MovieListInteractorProtocol: class {
    
    var apiManager: MovieDBAPIManager! { set get }
    
    var localDataManager: LocalDataManager! { set get }
    
    func loadData()
    
    func search(text: String)
}

protocol MovieListRouterProtocol: class {
    
    func showDetailScene()
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}

protocol MovieListWireframeProtocol: class {
    
    func configureModule(viewController: MovieListViewController)
}

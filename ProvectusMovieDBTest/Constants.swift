//
//  Constants.swift
//  ProvectusMovieDBTest
//
//  Created by Artem Shvets on 02.09.2018.
//  Copyright © 2018 Artem Shvets. All rights reserved.
//

import UIKit

class Constants: NSObject {
    typealias imageConfigSuccessBlock = (_ movieImageConfig : MovieImageConfiguration?) ->Void
    typealias movieGenresSuccessBlock = (_ genres : [GenresModel]) ->Void
    typealias movieListSuccessBlock = (_ movies : [MovieModel]) ->Void
    typealias movieManagedObjectListSuccessBlock = (_ movies : [Movie]) ->Void
    typealias movieSuccessBlock = (_ movie : MovieModel?) ->Void
    typealias failureBlock = (_ error : Error?) ->Void
}
